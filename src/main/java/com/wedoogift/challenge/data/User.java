package com.wedoogift.challenge.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity {
  private int id;
  private List<Balance> balance;
  private String username;
  private String password;
}
