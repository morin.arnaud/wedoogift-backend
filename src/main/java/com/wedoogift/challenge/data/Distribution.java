package com.wedoogift.challenge.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Distribution extends BaseEntity {

  private double amount;

  @JsonDeserialize(using = LocalDateDeserializer.class)
  @JsonProperty("start_date")
  private LocalDate startDate;

  @JsonDeserialize(using = LocalDateDeserializer.class)
  @JsonProperty("end_date")
  private LocalDate endDate;

  @JsonProperty("company_id")
  private int companyId;

  @JsonProperty("user_id")
  private int userId;

  @JsonProperty("wallet_id")
  private int walletId;
}
