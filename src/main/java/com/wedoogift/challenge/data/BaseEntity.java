package com.wedoogift.challenge.data;

import lombok.Getter;
import lombok.Setter;

public class BaseEntity {
  @Getter @Setter private int id;
}
