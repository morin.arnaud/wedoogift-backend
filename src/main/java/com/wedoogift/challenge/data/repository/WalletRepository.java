package com.wedoogift.challenge.data.repository;

import com.wedoogift.challenge.data.Wallet;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Repository
public class WalletRepository extends BaseRepository<Wallet> {

  @PostConstruct
  private void load() throws IOException {
    super.load("wallets", Wallet.class);
  }
}
