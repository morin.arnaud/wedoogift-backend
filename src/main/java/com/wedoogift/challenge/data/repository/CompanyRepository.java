package com.wedoogift.challenge.data.repository;

import com.wedoogift.challenge.data.Company;
import com.wedoogift.challenge.data.EntityNotFoundException;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Optional;

@Repository
public class CompanyRepository extends BaseRepository<Company> {

  @PostConstruct
  private void load() throws IOException {
    super.load("companies", Company.class);
  }

  public void substractGift(int companyId, double amount) {
    final Company company =
        findById(companyId)
            .orElseThrow(() -> new EntityNotFoundException(companyId, Company.class));
    company.setBalance(company.getBalance() - amount);
  }

  public Optional<Company> findByUsername(String username) {
    return this.datas.stream()
        .filter(company -> company.getUsername().equalsIgnoreCase(username))
        .findFirst();
  }
}
