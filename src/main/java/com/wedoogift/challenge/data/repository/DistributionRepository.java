package com.wedoogift.challenge.data.repository;

import com.wedoogift.challenge.data.Distribution;
import org.springframework.stereotype.Repository;

@Repository
public class DistributionRepository extends BaseRepository<Distribution> {

  private int idCounter = 0;

  public Distribution save(Distribution distribution) {
    distribution.setId(++idCounter);
    datas.add(distribution);
    return distribution;
  }
}
