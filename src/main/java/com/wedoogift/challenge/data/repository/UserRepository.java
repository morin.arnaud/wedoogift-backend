package com.wedoogift.challenge.data.repository;

import com.wedoogift.challenge.data.User;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Optional;

@Repository
public class UserRepository extends BaseRepository<User> {

  @PostConstruct
  private void load() throws IOException {
    super.load("users", User.class);
  }

  public Optional<User> findByUsername(String username) {
    return this.datas.stream()
        .filter(user -> user.getUsername().equalsIgnoreCase(username))
        .findFirst();
  }
}
