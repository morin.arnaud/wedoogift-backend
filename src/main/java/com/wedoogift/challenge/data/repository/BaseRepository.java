package com.wedoogift.challenge.data.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.challenge.data.BaseEntity;
import com.wedoogift.challenge.data.RepositoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BaseRepository<T extends BaseEntity> {

  @Autowired private ObjectMapper objectMapper;

  @Value("${data.input.path}")
  private String dataInputPath;

  @Autowired private RepositoryUtils repositoryUtils;

  protected final List<T> datas = new ArrayList<>();

  public void load(String jsonNode, Class<T> clazz) throws IOException {
    datas.addAll(repositoryUtils.load(jsonNode, clazz, dataInputPath));
  }

  public Optional<T> findById(int id) {
    return this.datas.stream().filter(u -> u.getId() == id).findFirst();
  }

  public List<T> findAll() {
    return datas;
  }
}
