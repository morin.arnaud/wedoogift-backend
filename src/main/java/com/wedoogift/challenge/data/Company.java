package com.wedoogift.challenge.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Company extends BaseEntity {
  private String name;
  private double balance;
  private String username;
  private String password;
}
