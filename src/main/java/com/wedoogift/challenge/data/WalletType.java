package com.wedoogift.challenge.data;

public enum WalletType {
  GIFT,
  FOOD;
}
