package com.wedoogift.challenge.data;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Component
public class RepositoryUtils {

  @Autowired private ObjectMapper objectMapper;

  public <T> List<T> load(String jsonNode, Class<T> clazz, String dataInputPath)
      throws IOException {
    final JsonNode jsonNodes = objectMapper.readTree(new File(dataInputPath));

    return objectMapper.readValue(
        jsonNodes.get(jsonNode).toString(),
        objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
  }
}
