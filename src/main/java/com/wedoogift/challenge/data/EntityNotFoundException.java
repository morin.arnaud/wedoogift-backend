package com.wedoogift.challenge.data;

public class EntityNotFoundException extends RuntimeException {
  public EntityNotFoundException(int id, Class<? extends BaseEntity> entityClass) {
    super(String.format("%s::%d not found", entityClass.getName(), id));
  }
}
