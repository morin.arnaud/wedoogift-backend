package com.wedoogift.challenge;

import com.wedoogift.challenge.data.*;
import com.wedoogift.challenge.data.repository.CompanyRepository;
import com.wedoogift.challenge.data.repository.DistributionRepository;
import com.wedoogift.challenge.data.repository.UserRepository;
import com.wedoogift.challenge.data.repository.WalletRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@org.springframework.stereotype.Service
@Log4j2
public class EndowmentService {

  @Autowired private UserRepository userRepository;
  @Autowired private WalletRepository walletRepository;
  @Autowired private CompanyRepository companyRepository;
  @Autowired private DistributionRepository distributionRepository;

  public Distribution distributeCard(
      int userId, int walletId, int companyId, double cardAmount, LocalDate startDate) {
    final Company company =
        companyRepository
            .findById(companyId)
            .orElseThrow(() -> new EntityNotFoundException(companyId, Company.class));
    final Wallet wallet =
        walletRepository
            .findById(walletId)
            .orElseThrow(() -> new EntityNotFoundException(walletId, Wallet.class));
    if (company.getBalance() - cardAmount > 0) {
      // When a database will be setup the following statement must be executed within a Transaction
      companyRepository.substractGift(companyId, cardAmount);
      return distributionRepository.save(
          Distribution.builder()
              .amount(cardAmount)
              .companyId(companyId)
              .userId(userId)
              .walletId(walletId)
              .startDate(startDate)
              .endDate(ttlStrategy(wallet.getType(), startDate))
              .build());
    } else {
      // FIXME Should be a dedicated Exception instead of using the generic unchecked one
      throw new RuntimeException(
          String.format(
              "Gift Card of %f has not been credited to user %d because Company %d don't have enough fund available.",
              cardAmount, userId, companyId));
    }
  }

  private LocalDate ttlStrategy(WalletType walletType, LocalDate startDate) {
    if (walletType == WalletType.GIFT) {
      return startDate.plusYears(1).minusDays(1);
    } else {
      final LocalDate nextEndOfFebruaryCandidate =
          YearMonth.of(startDate.getYear(), 2).atEndOfMonth();
      if (startDate.isBefore(nextEndOfFebruaryCandidate)) {
        return nextEndOfFebruaryCandidate;
      } else {
        return nextEndOfFebruaryCandidate.plusYears(1);
      }
    }
  }

  public Optional<Company> findCompanyByUsername(String username) {
    return companyRepository.findByUsername(username);
  }

  public Optional<User> findUserByUsername(String username) {
    return userRepository.findByUsername(username);
  }

  public List<Balance> calculateUserBalance(int userId, LocalDate referenceDate) {
    return userRepository
        .findById(userId)
        .map(
            user ->
                walletRepository.findAll().stream()
                    .map(wallet -> sumUserCards(user, wallet.getId(), referenceDate))
                    .filter(Optional::isPresent)
                    .map(Optional::get)
                    .collect(toList()))
        .orElseThrow(() -> new EntityNotFoundException(userId, User.class));
  }

  private Optional<Balance> sumUserCards(User user, int walletId, LocalDate referenceDate) {
    final double originalBalance = getOriginalBalance(user, walletId);

    final double balance =
        distributionRepository.findAll().stream()
            .filter(
                distribution ->
                    distribution.getUserId() == user.getId()
                        && distribution.getWalletId() == walletId)
            .filter(
                distribution ->
                    referenceDate.isAfter(distribution.getStartDate())
                        && referenceDate.isBefore(distribution.getEndDate()))
            .mapToDouble(Distribution::getAmount)
            .sum();
    return balance > 0
        ? Optional.of(
            Balance.builder().walletId(walletId).amount(originalBalance + balance).build())
        : Optional.empty();
  }

  private double getOriginalBalance(User user, int walletId) {
    return user.getBalance().stream()
        .filter(balance -> balance.getWalletId() == walletId)
        .findFirst()
        .map(Balance::getAmount)
        .orElse(0.0);
  }
}
