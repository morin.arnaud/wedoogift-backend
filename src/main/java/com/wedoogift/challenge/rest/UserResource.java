package com.wedoogift.challenge.rest;

import com.wedoogift.challenge.EndowmentService;
import com.wedoogift.challenge.data.Balance;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController()
@RequestMapping("/users")
class UserResource {

  private final EndowmentService endowmentService;

  UserResource(EndowmentService endowmentService) {
    this.endowmentService = endowmentService;
  }

  @GetMapping("/balance")
  @ResponseStatus(HttpStatus.OK)
  @PreAuthorize("hasRole('USER')")
  public List<Balance> getBalance(
      Authentication authentication,
      @RequestParam("reference_date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
          LocalDate referenceDate) {
    return endowmentService
        .findUserByUsername(authentication.getName())
        .map(user -> endowmentService.calculateUserBalance(user.getId(), referenceDate))
        .orElseThrow(
            () ->
                // FIXME Create a dedicated exception if needed
                new RuntimeException(
                    String.format(
                        "User with username %s doesn't exist", authentication.getName())));
  }
}
