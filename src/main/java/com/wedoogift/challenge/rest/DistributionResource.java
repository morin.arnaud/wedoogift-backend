package com.wedoogift.challenge.rest;

import com.wedoogift.challenge.EndowmentService;
import com.wedoogift.challenge.data.Distribution;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController()
@RequestMapping("/distributions")
class DistributionResource {

  private final EndowmentService endowmentService;

  DistributionResource(EndowmentService endowmentService) {
    this.endowmentService = endowmentService;
  }

  @PostMapping("")
  @ResponseStatus(HttpStatus.CREATED)
  @PreAuthorize("hasRole('COMPANY')")
  public Distribution create(
      Authentication authentication, @RequestBody NewDistribution newDistribution) {
    return endowmentService
        .findCompanyByUsername(authentication.getName())
        .map(
            company ->
                endowmentService.distributeCard(
                    newDistribution.getUserId(),
                    newDistribution.getWalletId(),
                    company.getId(),
                    newDistribution.getAmount(),
                    newDistribution.getStartDate()))
        .orElseThrow(
            () ->
                // FIXME Create a dedicated exception if needed
                new RuntimeException(
                    String.format(
                        "Company with username %s doesn't exist", authentication.getName())));
  }
}
