package com.wedoogift.challenge.rest;

import com.wedoogift.challenge.security.JWTTokenProvider;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/authenticate")
@Log4j2
public class AuthenticateResource {

  private final AuthenticationManager authenticationManager;
  private final JWTTokenProvider jwtTokenProvider;

  public AuthenticateResource(
      AuthenticationManager authenticationManager, JWTTokenProvider jwtTokenProvider) {
    this.authenticationManager = authenticationManager;
    this.jwtTokenProvider = jwtTokenProvider;
  }

  @PostMapping("")
  @ResponseStatus(HttpStatus.OK)
  public String authenticateUser(@RequestBody AuthenticateRequest authenticateRequest) {
    Authentication authentication =
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                authenticateRequest.getUserName(), authenticateRequest.getPassword()));
    return jwtTokenProvider.generateToken((UserDetails) authentication.getPrincipal());
  }
}
