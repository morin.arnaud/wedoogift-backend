package com.wedoogift.challenge.security;

import com.wedoogift.challenge.data.Company;
import com.wedoogift.challenge.data.User;
import com.wedoogift.challenge.data.repository.CompanyRepository;
import com.wedoogift.challenge.data.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserAuthDetailsService implements UserDetailsService {

  private final UserRepository userRepository;
  private final CompanyRepository companyRepository;

  public UserAuthDetailsService(
      UserRepository userRepository, CompanyRepository companyRepository) {
    this.userRepository = userRepository;
    this.companyRepository = companyRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    // FIXME In order to easily have 2 different authentications for User and Company I chose to
    // prefixe
    // the username by user_ or company_
    // Obviously it is not a good strategy for Production. All Users (Company & Client) must in the
    // same bucket
    UserDetails systemUser;
    if (username.startsWith("user_")) {
      final User user =
          userRepository
              .findByUsername(username)
              .orElseThrow(
                  () ->
                      new UsernameNotFoundException(
                          "User with username " + username + "Not Found"));
      systemUser =
          org.springframework.security.core.userdetails.User.withUsername(username)
              .password(user.getPassword())
              .roles(WedoogiftRole.USER.name())
              .build();
    } else {
      final Company company =
          companyRepository
              .findByUsername(username)
              .orElseThrow(
                  () ->
                      new UsernameNotFoundException(
                          "Company with username " + username + "Not Found"));
      systemUser =
          org.springframework.security.core.userdetails.User.withUsername(username)
              .password(company.getPassword())
              .roles(WedoogiftRole.COMPANY.name())
              .build();
    }
    return systemUser;
  }
}
