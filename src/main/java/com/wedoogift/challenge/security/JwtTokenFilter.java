package com.wedoogift.challenge.security;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
@Log4j2
public class JwtTokenFilter extends OncePerRequestFilter {

  private final JWTTokenProvider jwtTokenProvider;
  private final UserDetailsService authDetailsService;

  JwtTokenFilter(JWTTokenProvider jwtTokenProvider, UserDetailsService authDetailsService) {
    this.jwtTokenProvider = jwtTokenProvider;
    this.authDetailsService = authDetailsService;
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {

    Optional<String> jwt = getJwtFromRequest(request);
    if (jwt.isPresent() && StringUtils.hasText(jwt.get()) && jwtTokenProvider.validate(jwt.get())) {
      String userNameFromToken = jwtTokenProvider.getUserNameFromToken(jwt.get());
      UserDetails userDetails = authDetailsService.loadUserByUsername(userNameFromToken);
      UsernamePasswordAuthenticationToken authentication =
          new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
      SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    filterChain.doFilter(request, response);
  }

  private Optional<String> getJwtFromRequest(HttpServletRequest request) {
    String bearerToken = request.getHeader(HttpHeaders.AUTHORIZATION);
    if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
      return Optional.of(bearerToken.substring(7));
    }
    return Optional.empty();
  }
}
