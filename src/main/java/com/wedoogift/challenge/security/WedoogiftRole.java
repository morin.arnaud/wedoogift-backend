package com.wedoogift.challenge.security;

public enum WedoogiftRole {
  USER,
  COMPANY
}
