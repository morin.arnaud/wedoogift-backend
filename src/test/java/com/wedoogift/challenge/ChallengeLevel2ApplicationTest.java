package com.wedoogift.challenge;

import com.wedoogift.challenge.data.Company;
import com.wedoogift.challenge.data.Distribution;
import com.wedoogift.challenge.data.RepositoryUtils;
import com.wedoogift.challenge.data.User;
import com.wedoogift.challenge.data.repository.CompanyRepository;
import com.wedoogift.challenge.data.repository.DistributionRepository;
import com.wedoogift.challenge.data.repository.UserRepository;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

@SpringBootTest
class ChallengeLevel2ApplicationTest {

  @Autowired EndowmentService endowmentService;
  @Autowired CompanyRepository companyRepository;
  @Autowired UserRepository userRepository;
  @Autowired DistributionRepository distributionRepository;
  @Autowired RepositoryUtils repositoryUtils;

  @Test
  void shouldOutputExpectedValues() throws Exception {
    // When
    final LocalDate now = LocalDate.of(2021, 2, 1);
    endowmentService.distributeCard(1, 1, 1, 50, LocalDate.of(2020, 9, 16));
    endowmentService.distributeCard(2, 1, 1, 100, LocalDate.of(2020, 8, 1));
    endowmentService.distributeCard(3, 1, 2, 1000, LocalDate.of(2020, 5, 1));
    endowmentService.distributeCard(1, 2, 1, 250, LocalDate.of(2020, 5, 1));

    final List<User> refreshedUsers =
        userRepository.findAll().stream()
            .map(
                user ->
                    User.builder()
                        .id(user.getId())
                        .balance(endowmentService.calculateUserBalance(user.getId(), now))
                        .build())
            .collect(toList());

    // Then
    final String outputPath = "src/test/resources/level2_output_expected.json";
    assertThat(companyRepository.findAll())
        .extracting("name", "balance")
        .contains(
            repositoryUtils.load("companies", Company.class, outputPath).stream()
                .map(c -> tuple(c.getName(), c.getBalance()))
                .toArray(Tuple[]::new));
    assertThat(refreshedUsers).containsAll(repositoryUtils.load("users", User.class, outputPath));
    assertThat(distributionRepository.findAll())
        .containsAll(repositoryUtils.load("distributions", Distribution.class, outputPath));
  }
}
