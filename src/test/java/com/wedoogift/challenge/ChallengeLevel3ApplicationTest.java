package com.wedoogift.challenge;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.challenge.data.*;
import com.wedoogift.challenge.data.repository.CompanyRepository;
import com.wedoogift.challenge.data.repository.DistributionRepository;
import com.wedoogift.challenge.data.repository.UserRepository;
import com.wedoogift.challenge.rest.AuthenticateRequest;
import com.wedoogift.challenge.rest.NewDistribution;
import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ChallengeLevel3ApplicationTest {

  @Autowired private MockMvc mockMvc;

  @Autowired private ObjectMapper objectMapper;

  @Autowired CompanyRepository companyRepository;
  @Autowired UserRepository userRepository;
  @Autowired DistributionRepository distributionRepository;
  @Autowired RepositoryUtils repositoryUtils;

  @Test
  void shouldOutputExpectedValues() throws Exception {
    final String wedoogiftToken = retrieveToken("company_wedoogift", "wedoogift_password");
    final String wedoofoodToken = retrieveToken("company_wedoofood", "wedoofood_password");
    final String user1Token = retrieveToken("user_user1", "user1_password");
    final String user2Token = retrieveToken("user_user2", "user2_password");
    final String user3Token = retrieveToken("user_user3", "user3_password");

    // When
    final LocalDate now = LocalDate.of(2021, 2, 1);

    distributeCard(wedoogiftToken, new NewDistribution(1, 1, 50, LocalDate.of(2020, 9, 16)));
    distributeCard(wedoogiftToken, new NewDistribution(2, 1, 100, LocalDate.of(2020, 8, 1)));
    distributeCard(wedoofoodToken, new NewDistribution(3, 1, 1000, LocalDate.of(2020, 5, 1)));
    distributeCard(wedoogiftToken, new NewDistribution(1, 2, 250, LocalDate.of(2020, 5, 1)));

    final List<User> refreshedUsers =
        Arrays.asList(
            User.builder().id(1).balance(getUserBalance(user1Token, now)).build(),
            User.builder().id(2).balance(getUserBalance(user2Token, now)).build(),
            User.builder().id(3).balance(getUserBalance(user3Token, now)).build());

    // Then
    final String outputPath = "src/test/resources/level2_output_expected.json";
    assertThat(companyRepository.findAll())
        .extracting("name", "balance")
        .contains(
            repositoryUtils.load("companies", Company.class, outputPath).stream()
                .map(c -> tuple(c.getName(), c.getBalance()))
                .toArray(Tuple[]::new));
    assertThat(distributionRepository.findAll())
        .containsAll(repositoryUtils.load("distributions", Distribution.class, outputPath));
    assertThat(refreshedUsers)
        .extracting("id", "balance")
        .contains(
            repositoryUtils.load("users", User.class, outputPath).stream()
                .map(user -> tuple(user.getId(), user.getBalance()))
                .toArray(Tuple[]::new));
  }

  private String retrieveToken(String username, String password) throws Exception {
    final MvcResult authenticate =
        this.mockMvc
            .perform(
                post("/authenticate")
                    .contentType("application/json")
                    .content(
                        objectMapper.writeValueAsString(
                            AuthenticateRequest.builder()
                                .userName(username)
                                .password(password)
                                .build())))
            .andExpect(status().isOk())
            .andReturn();
    return authenticate.getResponse().getContentAsString();
  }

  private void distributeCard(String jwtToken, NewDistribution newDistribution) throws Exception {
    this.mockMvc
        .perform(
            post("/distributions")
                .contentType("application/json")
                .header("Authorization", "Bearer " + jwtToken)
                .content(objectMapper.writeValueAsString(newDistribution)))
        .andExpect(status().isCreated());
  }

  private List<Balance> getUserBalance(String jwtToken, LocalDate referenceDate) throws Exception {
    return extractResponseList(
        this.mockMvc
            .perform(
                get("/users/balance")
                    .header("Authorization", "Bearer " + jwtToken)
                    .param("reference_date", referenceDate.toString()))
            .andExpect(status().isOk())
            .andReturn(),
        Balance.class);
  }

  private <T> List<T> extractResponseList(MvcResult responseBody, Class<T> clazz)
      throws com.fasterxml.jackson.core.JsonProcessingException, UnsupportedEncodingException {
    return objectMapper.readValue(
        responseBody.getResponse().getContentAsString(),
        objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
  }
}
