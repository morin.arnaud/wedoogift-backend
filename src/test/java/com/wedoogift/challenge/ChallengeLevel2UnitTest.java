package com.wedoogift.challenge;

import com.wedoogift.challenge.data.*;
import com.wedoogift.challenge.data.repository.CompanyRepository;
import com.wedoogift.challenge.data.repository.DistributionRepository;
import com.wedoogift.challenge.data.repository.UserRepository;
import com.wedoogift.challenge.data.repository.WalletRepository;
import lombok.Builder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static com.wedoogift.challenge.data.WalletType.FOOD;
import static com.wedoogift.challenge.data.WalletType.GIFT;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ChallengeLevel2UnitTest {

  @Mock DistributionRepository distributionRepository;
  @Mock CompanyRepository companyRepository;
  @Mock WalletRepository walletRepository;
  @Mock UserRepository userRepository;
  @InjectMocks EndowmentService endowmentService = new EndowmentService();
  public static final Wallet GIFT_WALLET = Wallet.builder().type(GIFT).build();
  public static final Wallet FOOD_WALLET = Wallet.builder().type(FOOD).build();

  static {
    GIFT_WALLET.setId(1);
    FOOD_WALLET.setId(2);
  }

  @Builder
  static class Given {
    int userId;
    Wallet wallet;
    int companyId;
    int userGiftBalance;
    int giftAmount;
    LocalDate startDate;
    int companyBalance;
  }

  final Given.GivenBuilder givenBuilder =
      Given.builder()
          .userId(1)
          .userGiftBalance(100)
          .companyId(2)
          .startDate(LocalDate.of(2021, 3, 29));

  @Test
  void should_save_GIFT_distribution_When_company_has_enough_money() {
    // Given
    final Given given = givenBuilder.wallet(GIFT_WALLET).giftAmount(80).companyBalance(100).build();

    // When
    final Distribution actualDistribution = when_save_cards_distribution(given);

    // Then
    assertThat(actualDistribution.getCompanyId()).isEqualTo(given.companyId);
    assertThat(actualDistribution.getWalletId()).isEqualTo(given.wallet.getId());
    assertThat(actualDistribution.getUserId()).isEqualTo(given.userId);
    assertThat(actualDistribution.getAmount()).isEqualTo(given.giftAmount);
    assertThat(actualDistribution.getStartDate()).isEqualTo(given.startDate);
    assertThat(actualDistribution.getEndDate())
        .isEqualTo(given.startDate.plusYears(1).minusDays(1));
  }

  @Test
  void should_save_FOOD_distribution_for_SAME_year_When_company_has_enough_money() {

    // Given
    final Given given =
        this.givenBuilder
            .wallet(FOOD_WALLET)
            .giftAmount(80)
            .companyBalance(100)
            .startDate(LocalDate.of(2021, 3, 29))
            .build();

    // When
    final Distribution actualDistribution = when_save_cards_distribution(given);

    // Then
    assertThat(actualDistribution.getCompanyId()).isEqualTo(given.companyId);
    assertThat(actualDistribution.getWalletId()).isEqualTo(given.wallet.getId());
    assertThat(actualDistribution.getUserId()).isEqualTo(given.userId);
    assertThat(actualDistribution.getAmount()).isEqualTo(given.giftAmount);
    assertThat(actualDistribution.getStartDate()).isEqualTo(given.startDate);
    assertThat(actualDistribution.getEndDate()).isEqualTo(LocalDate.of(2022, 2, 28));
  }

  @Test
  void should_save_FOOD_distribution_for_NEXT_year_When_company_has_enough_money() {

    // Given
    final Given given =
        this.givenBuilder
            .wallet(FOOD_WALLET)
            .giftAmount(80)
            .companyBalance(100)
            .startDate(LocalDate.of(2021, 1, 29))
            .build();
    // When
    Distribution actualDistribution = when_save_cards_distribution(given);

    // Then
    assertThat(actualDistribution.getStartDate()).isEqualTo(given.startDate);
    assertThat(actualDistribution.getEndDate()).isEqualTo(LocalDate.of(2021, 2, 28));
  }

  private Distribution when_save_cards_distribution(Given given) {
    when(companyRepository.findById(anyInt()))
        .thenReturn(Optional.of(Company.builder().balance(given.companyBalance).build()));
    when(walletRepository.findById(given.wallet.getId())).thenReturn(Optional.of(given.wallet));

    // When
    endowmentService.distributeCard(
        given.userId, given.wallet.getId(), given.companyId, given.giftAmount, given.startDate);

    // Then
    verify(companyRepository, times(1)).substractGift(given.companyId, given.giftAmount);

    ArgumentCaptor<Distribution> argument = ArgumentCaptor.forClass(Distribution.class);
    verify(distributionRepository, times(1)).save(argument.capture());

    final Distribution actualDistribution = argument.getValue();
    return actualDistribution;
  }

  @Test
  void should_NOT_save_distribution_When_company_has_NOT_enough_money() {
    // Given
    final Given given =
        givenBuilder.wallet(GIFT_WALLET).giftAmount(120).companyBalance(100).build();
    when(companyRepository.findById(anyInt()))
        .thenReturn(Optional.of(Company.builder().balance(given.companyBalance).build()));

    // When // Then
    assertThatThrownBy(
            () -> {
              endowmentService.distributeCard(
                  given.userId,
                  given.wallet.getId(),
                  given.companyId,
                  given.giftAmount,
                  given.startDate);
            })
        .isInstanceOf(RuntimeException.class);
  }

  @Test
  void should_return_uptodate_user_balance() {
    // Given
    final Given given =
        givenBuilder.wallet(FOOD_WALLET).giftAmount(120).companyBalance(100).build();
    final LocalDate now = LocalDate.of(2021, 3, 27);
    when(walletRepository.findAll()).thenReturn(Arrays.asList(GIFT_WALLET, FOOD_WALLET));
    when(userRepository.findById(given.userId))
        .thenReturn(
            Optional.of(
                User.builder()
                    .id(given.userId)
                    .balance(
                        Arrays.asList(
                            Balance.builder()
                                .amount(given.userGiftBalance)
                                .walletId(given.wallet.getId())
                                .build()))
                    .build()));
    final Distribution.DistributionBuilder distributionBuilder =
        Distribution.builder().userId(given.userId).walletId(given.wallet.getId());
    when(distributionRepository.findAll())
        .thenReturn(
            Arrays.asList(
                // Gift Card expired
                decorateDates(distributionBuilder.amount(10), now.minusDays(2)),
                // Gift Card OK
                decorateDates(distributionBuilder.amount(100), now.plusDays(2)),
                // Gift Card OK
                decorateDates(distributionBuilder.amount(90), now.plusMonths(5)),
                // Gift Card available in the future
                decorateDates(distributionBuilder.amount(80), now.plusYears(2)),
                // OK but for another user
                decorateDates(Distribution.builder().userId(2).amount(70), now.plusDays(2))));

    // When
    final List<Balance> userBalance = endowmentService.calculateUserBalance(given.userId, now);

    // Then
    assertThat(userBalance)
        .containsExactly(
            Balance.builder()
                .amount(given.userGiftBalance + 190)
                .walletId(given.wallet.getId())
                .build());
  }

  private Distribution decorateDates(Distribution.DistributionBuilder builder, LocalDate endDate) {
    return builder.endDate(endDate).startDate(endDate.minusYears(1)).build();
  }
}
